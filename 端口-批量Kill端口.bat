@echo off
setlocal enabledelayedexpansion
chcp 65001

set list= 8001,8002

echo 开始批量kill端口%list%

(for %%p in (%list%) do (
    echo -----------------------------------端口%%p-----------------------------------------------------
    for /f "tokens=2,4,5" %%a in ('netstat -ano^|findstr ":%%p"') do (
        set temp=%%a
        
        for /f "usebackq delims=: tokens=1,2" %%i in (`set temp`) do (
	     if %%b equ LISTENING (
	        :: 取:后的端口赋值给j
	        if %%j==%%p (
	            echo 查找到监听端口 [%%p] 的pid: [%%c]
	            :: 执行kill命令
	            TASKKILL /PID %%c /F
	        )
	     )
	)
    )
    echo ------------------------------------------------------------------------------------------------
))

pause


